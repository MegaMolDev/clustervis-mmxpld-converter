#include "particle_data_manager.h"
#include <iostream>
#include <algorithm>

float particle_data_manager::particleRadius = 0.0f;
float particle_data_manager::particleRho = 0.0f;
particle_data_manager::dataTypes particle_data_manager::currentDataType = BUBBLE;
std::vector<float> particle_data_manager::localMinVelocity(3, std::numeric_limits<float>::max());
std::vector<float> particle_data_manager::localMaxVelocity(3, std::numeric_limits<float>::min());
float particle_data_manager::localMinVelLength = std::numeric_limits<float>::max();
float particle_data_manager::localMaxVelLength = std::numeric_limits<float>::min();
float particle_data_manager::localMinAngle = std::numeric_limits<float>::max();
float particle_data_manager::localMaxAngle = std::numeric_limits<float>::min();

particle_data_manager::particle_data_manager(void): number_frames(), 
													boundingbox(),
													particleCount(),
													globalMinVelocity(3, std::numeric_limits<float>::max()), globalMaxVelocity(3, std::numeric_limits<float>::min()),
													globalMinVelLength(std::numeric_limits<float>::max()), globalMaxVelLength(std::numeric_limits<float>::min()),
													globalMinAngle(std::numeric_limits<float>::max()), globalMaxAngle(std::numeric_limits<float>::min()) {
}


particle_data_manager::~particle_data_manager(void) {
}

template<class T> void particle_data_manager::change_endianness(T * value)	{
	unsigned char* c = reinterpret_cast<unsigned char*>(value);
	std::reverse(c, c + sizeof(*value));
}

bool particle_data_manager::convertBubbleToOwn(particle_bubble_data& from_rec, particle_own_data& to_rec)	{
	to_rec.xc = static_cast<float>(from_rec.xc);
	to_rec.yc = static_cast<float>(from_rec.yc);
	to_rec.zc = static_cast<float>(from_rec.zc);

	to_rec.uc = static_cast<float>(from_rec.uc);
	to_rec.vc = static_cast<float>(from_rec.vc);
	to_rec.wc = static_cast<float>(from_rec.wc);

	to_rec.o1 = static_cast<float>(from_rec.o1);
	to_rec.o2 = static_cast<float>(from_rec.o2);
	to_rec.o3 = static_cast<float>(from_rec.o3);

	to_rec.f1 = static_cast<float>(from_rec.fx_gl);
	to_rec.f2 = static_cast<float>(from_rec.fy_gl);
	to_rec.f3 = static_cast<float>(from_rec.fz_gl);
	to_rec.f4 = static_cast<float>(from_rec.fx_c_gl);
	to_rec.f5 = static_cast<float>(from_rec.fy_c_gl);
	to_rec.f6 = static_cast<float>(from_rec.fz_c_gl);

	float epsilon = 0.00001f;
	if((abs(from_rec.r - particleRadius)) >= epsilon) {// || (abs(from_rec.rho - particleRho)) >= epsilon )	{
		return false;	// only happens if all data == 0
	}

	if (abs(from_rec.xc) <= epsilon && abs(from_rec.yc) <= epsilon && abs(from_rec.zc) <= epsilon) {
		return false;
	}
	
	std::vector<float> velocity(3); velocity[0] = to_rec.uc; velocity[1] = to_rec.vc; velocity[2] = to_rec.wc;
	for (int c = 0; c<3; ++c) {
		if (velocity[c] < globalMinVelocity[c]) {
			globalMinVelocity[c] = velocity[c];
		}
		if (velocity[c] > globalMaxVelocity[c]) {
			globalMaxVelocity[c] = velocity[c];
		}
		if (velocity[c] < localMinVelocity[c]) {
			localMinVelocity[c] = velocity[c];
		}
		if (velocity[c] > localMaxVelocity[c]) {
			localMaxVelocity[c] = velocity[c];
		}
	}
	
	float velLength = std::sqrt(velocity[0]*velocity[0] + velocity[1]*velocity[1] + velocity[2]*velocity[2]);
	globalMinVelLength = std::min(velLength, globalMinVelLength);
	globalMaxVelLength = std::max(velLength, globalMaxVelLength);
	localMinVelLength = std::min(velLength, localMinVelLength);
	localMaxVelLength = std::max(velLength, localMaxVelLength);

	float rotationAngle = std::sqrt(std::pow(to_rec.o1,2) + std::pow(to_rec.o2,2) + std::pow(to_rec.o3,2));
	/*if (to_rec.o3 < 0.0f) { // TODO: hier ist vielleicht der Vergleich mit einer anderen Achse sinnvoll
		rotationAngle *= -1.0f;
	}*/
	if (rotationAngle < globalMinAngle) {
		globalMinAngle = rotationAngle;
	}
	if (rotationAngle > globalMaxAngle) {
		globalMaxAngle = rotationAngle;
	}
	if (rotationAngle < localMinAngle) {
		localMinAngle = rotationAngle;
	}
	if (rotationAngle > localMaxAngle) {
		localMaxAngle = rotationAngle;
	}

	return true;
}

bool particle_data_manager::convertChannelToOwn(particle_channel_data& from_rec, particle_own_data& to_rec)	{
	to_rec.xc = static_cast<float>(from_rec.xc);
	to_rec.yc = static_cast<float>(from_rec.yc);
	to_rec.zc = static_cast<float>(from_rec.zc);

	to_rec.uc = static_cast<float>(from_rec.uc);
	to_rec.vc = static_cast<float>(from_rec.vc);
	to_rec.wc = static_cast<float>(from_rec.wc);

	to_rec.o1 = static_cast<float>(from_rec.o1);
	to_rec.o2 = static_cast<float>(from_rec.o2);
	to_rec.o3 = static_cast<float>(from_rec.o3);

	to_rec.f1 = static_cast<float>(from_rec.ml1);
	to_rec.f2 = static_cast<float>(from_rec.ml2);
	to_rec.f3 = static_cast<float>(from_rec.ml3);
	to_rec.f4 = static_cast<float>(from_rec.ma1);
	to_rec.f5 = static_cast<float>(from_rec.ma2);
	to_rec.f6 = static_cast<float>(from_rec.ma3);

	float epsilon = 0.00001f;
	if((abs(from_rec.r - particleRadius)) >= epsilon || (abs(from_rec.rho - particleRho)) >= epsilon )	{
		return false;	// only happens if all data == 0
	}
	
	std::vector<float> velocity(3); velocity[0] = to_rec.uc; velocity[1] = to_rec.vc; velocity[2] = to_rec.wc;
	for (int c = 0; c<3; ++c) {
		if (velocity[c] < globalMinVelocity[c]) {
			globalMinVelocity[c] = velocity[c];
		}
		if (velocity[c] > globalMaxVelocity[c]) {
			globalMaxVelocity[c] = velocity[c];
		}
		if (velocity[c] < localMinVelocity[c]) {
			localMinVelocity[c] = velocity[c];
		}
		if (velocity[c] > localMaxVelocity[c]) {
			localMaxVelocity[c] = velocity[c];
		}
	}

	float velLength = std::sqrt(velocity[0]*velocity[0] + velocity[1]*velocity[1] + velocity[2]*velocity[2]);
	globalMinVelLength = std::min(velLength, globalMinVelLength);
	globalMaxVelLength = std::max(velLength, globalMaxVelLength);
	localMinVelLength = std::min(velLength, localMinVelLength);
	localMaxVelLength = std::max(velLength, localMaxVelLength);

	float rotationAngle = std::sqrt(std::pow(to_rec.o1,2) + std::pow(to_rec.o2,2) + std::pow(to_rec.o3,2));
	/*if (to_rec.o3 < 0.0f) {
		rotationAngle *= -1.0f;
	}*/
	if (rotationAngle < globalMinAngle) {
		globalMinAngle = rotationAngle;
	}
	if (rotationAngle > globalMaxAngle) {
		globalMaxAngle = rotationAngle;
	}
	if (rotationAngle < localMinAngle) {
		localMinAngle = rotationAngle;
	}
	if (rotationAngle > localMaxAngle) {
		localMaxAngle = rotationAngle;
	}

	return true;
}

bool particle_data_manager::convertBubble(std::string& from_path, std::string& own_to_file, int start_time_step, int end_time_step)	{
	//Bounding box of bubbleparticles
	boundingbox.push_back(0.0f);			// minX
	boundingbox.push_back(-0.00215517f);	// minY
	boundingbox.push_back(0.0f);			// minZ
	boundingbox.push_back(4.41344f);		// maxX
	boundingbox.push_back(1.00431f);		// maxY
	boundingbox.push_back(2.20672f);		// maxZ
	particleRadius = 0.02586f;
	particleRho = 0.001f;

	currentDataType = BUBBLE;

	particleCount = 2880 - 8;	// only 7 "zero"-particles

	std::vector<particle_bubble_data> bubble_particles;
	std::vector<particle_own_data> own_particles;

	particle_bubble_data bubble_data;
	particle_own_data own_data;

	clearFile(own_to_file);
	number_frames = end_time_step - start_time_step + 1;

	//header + seek table for MMPLD format
	addHeaderToFile(own_to_file);
	addSeekTableToFile(own_to_file);

	for (int timeStep = start_time_step; timeStep <= end_time_step; ++timeStep)	{
		std::cout << timeStep-start_time_step+1 << "/" << number_frames << std::endl;
		bubble_particles.clear();
		own_particles.clear();

		localMinVelocity.clear();
		localMinVelocity.resize(3, std::numeric_limits<float>::max());
		localMaxVelocity.clear();
		localMaxVelocity.resize(3, std::numeric_limits<float>::min());
		localMinVelLength = std::numeric_limits<float>::max();
		localMaxVelLength = std::numeric_limits<float>::min();
		localMinAngle = std::numeric_limits<float>::max();
		localMaxAngle = std::numeric_limits<float>::min();

		if (!loadFromBubbleFile(from_path,timeStep, bubble_particles))	{
			return false;
		}
		
		for (unsigned int idx = 0; idx < bubble_particles.size(); ++idx)	{
			bubble_data = bubble_particles[idx];
			if(convertBubbleToOwn(bubble_data, own_data) && 
				(idx != 780 && idx != 969 && idx != 986 && idx != 1094 &&
					idx != 1358 && idx != 1459 && idx != 2833 && idx != 2835))	{ // particle radius & co not all == 0
				own_particles.push_back(own_data);
			}
			else {
				if (idx != 780 && idx != 969 && idx != 986 && idx != 1094 &&
					idx != 1358 && idx != 1459 && idx != 2833 && idx != 2835) {
					std::cout << idx << ", " << std::endl;
				}
			}
		}

		std::cout << bubble_particles[0].tp << std:: endl;
		// save time step
		if (!saveTimeStepToFile(own_to_file, bubble_particles[0].tp , own_particles))	{
			return false;
		}
	}

	for (int i = 0; i<3; ++i) {
		std::cout << globalMinVelocity[i] << ", ";
	}
	std::cout << std::endl;
	for (int i = 0; i<3; ++i) {
		std::cout << globalMaxVelocity[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << globalMinVelLength << ", " << globalMaxVelLength << std::endl;
	std::cout << globalMinAngle << ", " << globalMaxAngle << std::endl;

	modifyMinMaxVelocityMinMaxAngle(own_to_file);

	std::cout << "Fertig!" << std::endl;

	return true;
}

bool particle_data_manager::convertChannel(std::string& from_path, std::string& own_to_file, int start_time_step, int end_time_step)
{
	//Bounding box of channelparticles
	boundingbox.push_back(0.0f);			// minX
	boundingbox.push_back(0.0f);			// minY
	boundingbox.push_back(-3.08216e-005f);	// minZ
	boundingbox.push_back(24.0481f);		// maxX
	boundingbox.push_back(0.829015f);		// maxY
	boundingbox.push_back(6.002f);			// maxZ
	particleRadius = 0.0555556f;
	particleRho = 1116.11902f;

	currentDataType = CHANNEL;

	particleCount = 27000 - PARTICLE_CHANNEL_FIXED_COUNT;

	std::vector<particle_channel_data> channel_particles;
	std::vector<particle_channel_data>::iterator it;
	std::vector<particle_channel_data> fixed_channel_particles(PARTICLE_CHANNEL_FIXED_COUNT);
	std::vector<particle_own_data> own_particles;
	std::vector<particle_own_data> fixed_own_particles;
	std::vector<unsigned __int64> null_particle_indizes;
	
	particle_channel_data channel_data;
	particle_own_data own_data;
	std::vector<double> times;

	loadTimestepTimes(from_path + "\\info_time_binaries.dat",  times);

	clearFile(own_to_file);
	number_frames = end_time_step - start_time_step + 1;

	// remember the fixed particles
	if (!loadFromChannelFile(from_path,start_time_step, channel_particles))	{
		return false;
	}
	// header + seek table for MMPLD format -> fixed particles are stored seperately
	std::string output_fixed = "D:\\Uni\\Diplom\\source\\MegaMol\\data\\kanal_fixed.mmpld";
	clearFile(output_fixed);
	addHeaderToFile(output_fixed, false);
	addSeekTableToFile(output_fixed, false);

	fixed_channel_particles.clear();
	fixed_own_particles.clear();

	// ensure to copy only available items
	int cnt = std::min(PARTICLE_CHANNEL_FIXED_COUNT,(int)channel_particles.size());
	fixed_channel_particles.insert(fixed_channel_particles.end(), channel_particles.begin(), channel_particles.begin() + cnt);
			
	for (unsigned int idx = 0; idx < fixed_channel_particles.size(); ++idx)	{
		channel_data = fixed_channel_particles[idx];
		convertChannelToOwn(channel_data, own_data);
		fixed_own_particles.push_back(own_data);
	}
	saveTimeStepToFile(output_fixed, -1.0f, fixed_own_particles, true);

	// get null particle indices
	if (!loadFromChannelFile(from_path,end_time_step, channel_particles))	{
		return false;
	}
		
	// delete the fixed particles from the beginning of the vector; all time steps behind PARTICLE_FOREIGN_FIRST_TIMESTEP_WITHOUT_FIXED_PARTICLES
	// does not include the fixed particles for memory space reason -> fixed particles are stored seperately
	if (end_time_step < PARTICLE_CHANNEL_FIRST_TIMESTEP_WITHOUT_FIXED_PARTICLES)	{
		int cnt = std::min(PARTICLE_CHANNEL_FIXED_COUNT,(int)fixed_channel_particles.size());
		channel_particles.erase(channel_particles.begin(), channel_particles.begin() + cnt);						
	}
	for (unsigned int idx = 0; idx < channel_particles.size(); ++idx)	{
		channel_data = channel_particles[idx];
		if (!convertChannelToOwn(channel_data, own_data))	{ // particle radius & co all == 0
			null_particle_indizes.push_back(static_cast<unsigned __int64>(idx));
		}
	}
	particleCount -= null_particle_indizes.size();
	std::sort(null_particle_indizes.begin(), null_particle_indizes.end());

	// header + seek table for MMXPLD format
	addHeaderToFile(own_to_file);
	addSeekTableToFile(own_to_file);

	// the mobile particles
	for (int timeStep = start_time_step; timeStep <= end_time_step; ++timeStep)	{
		std::cout << timeStep-start_time_step+1 << "/" << number_frames << std::endl;
		channel_particles.clear();
		own_particles.clear();
		
		localMinVelocity.clear();
		localMinVelocity.resize(3, std::numeric_limits<float>::max());
		localMaxVelocity.clear();
		localMaxVelocity.resize(3, std::numeric_limits<float>::min());
		localMinVelLength = std::numeric_limits<float>::max();
		localMaxVelLength = std::numeric_limits<float>::min();
		localMinAngle = std::numeric_limits<float>::max();
		localMaxAngle = std::numeric_limits<float>::min();

		if (!loadFromChannelFile(from_path,timeStep, channel_particles))	{
			return false;
		}
		
		// delete the fixed particles from the beginning of the vector; all time steps behind PARTICLE_FOREIGN_FIRST_TIMESTEP_WITHOUT_FIXED_PARTICLES
		// does not include the fixed particles for memory space reason -> fixed particles are stored seperately
		if (timeStep < PARTICLE_CHANNEL_FIRST_TIMESTEP_WITHOUT_FIXED_PARTICLES)	{
			int cnt = std::min(PARTICLE_CHANNEL_FIXED_COUNT,(int)fixed_channel_particles.size());
			channel_particles.erase(channel_particles.begin(), channel_particles.begin() + cnt);						
		}
		
		for (unsigned int idx = 0; idx < channel_particles.size(); ++idx)	{
			channel_data = channel_particles[idx];
			if (!std::binary_search(null_particle_indizes.begin(),null_particle_indizes.end(), idx) && convertChannelToOwn(channel_data, own_data))	{ // particle radius & co not all == 0
				own_particles.push_back(own_data);
			}
		}

		// save time step
		if (!saveTimeStepToFile(own_to_file, static_cast<float>(times[timeStep]), own_particles, false))	{
			return false;
		}
	}

	for (int i = 0; i<3; ++i) {
		std::cout << globalMinVelocity[i] << ", ";
	}
	std::cout << std::endl;
	for (int i = 0; i<3; ++i) {
		std::cout << globalMaxVelocity[i] << ", ";
	}
	std::cout << std::endl;
	std::cout << globalMinAngle << ", " << globalMaxAngle << std::endl;

	modifyMinMaxVelocityMinMaxAngle(own_to_file);

	std::cout << "Fertig!" << std::endl;

	return true;
}

bool particle_data_manager::loadTimestepTimes(std::string& file_name, std::vector<double> &times)
{
	std::string line;
	std::ifstream file(file_name); 
	unsigned int ts;
	double time;

	times.clear();

	while (getline(file,line))	{
		std::istringstream is(line);
	    
	    is >> ts >> time;

		times.push_back(time);
	}	        
	    
	times.resize(426);
	file.close();

	return true;
}

bool particle_data_manager::loadFromBubbleFile(std::string& folder, int time_step, std::vector<particle_bubble_data> &particles)	{
	std::string fileName = folder + "\\set_";
	std::string step = std::to_string(time_step);
	if (time_step < 25000) {
		fileName.append("00");
	}
	fileName.append(step + ".bin");
		
	particle_bubble_data pd_entry;
	int file_size;
	char *data;
	int data_size;
	int pos;	// position in data
	int number_of_particles;	// number of particles to read from file
	int current_particle;	// particle to read

	particles.clear();
		
	std::ifstream input_file (fileName, std::ios::in | std::ios::binary | std::ios::ate);
	
	if (!input_file.is_open())	{
		return false;
	}
		
	file_size = static_cast<int>(input_file.tellg());
	data = new char[file_size];

	// read file from the beginning
	input_file.seekg(0, std::ios::beg);
	input_file.read(data, file_size);

	// get record size	-> first entry: number of bytes of particledata	
	memcpy(&data_size, data, sizeof(data_size));
	
	pos = sizeof(data_size);	// offset -> begin of particledata
	current_particle = 0;
	number_of_particles = (data_size) / PARTICLE_BUBBLE_DATA_ITEM_SIZE;	//TODO: entspricht immer der Anzahl der Partikel...
	
	while (current_particle < number_of_particles)	// has more data
	{
		memcpy(&pd_entry, data + pos, PARTICLE_BUBBLE_DATA_HEADER_SIZE);

		particles.push_back(pd_entry);	

		pos += PARTICLE_BUBBLE_DATA_ITEM_SIZE;	// pos to next entry
		++current_particle;
	}

	delete[] data;		

	input_file.close();
	
	return true;
}


bool particle_data_manager::loadFromChannelFile(std::string& folder, int time_step, std::vector<particle_channel_data> &particles)
{
	std::string fileName = folder + "\\par_";
	std::string step = std::to_string(time_step);
		
	for (unsigned int i = 0; i < (6 - step.length()); i++)	{
		fileName.append("0");
	}

	fileName.append(step + ".bin");
		
	particle_channel_data pd_entry;
	int file_size;
	char *data;
	int data_size;
	int pos;	// position in data
	int number_of_particles;	// number of particles to read from file
	int current_particle;	// particle to read

	particles.clear();
		
	std::ifstream input_file (fileName, std::ios::in | std::ios::binary | std::ios::ate);
	
	if (!input_file.is_open())	{
		return false;
	}
		
	file_size = static_cast<int>(input_file.tellg());
	data = new char[file_size];

	// read file from the beginning
	input_file.seekg(0, std::ios::beg);
	input_file.read(data, file_size);

	// get record size	-> first entry: number of bytes of particledata	
	memcpy(&data_size, data, sizeof(data_size));
	change_endianness(&data_size);

	pos = sizeof(data_size);	// offset -> begin of particledata
	current_particle = 0;
	number_of_particles = (data_size) / PARTICLE_CHANNEL_DATA_ITEM_SIZE;
	
	while (current_particle < number_of_particles)	// has more data
	{
		memcpy(&pd_entry, data + pos, PARTICLE_CHANNEL_DATA_HEADER_SIZE);
		
		change_endianness(&(pd_entry.id));
		change_endianness(&(pd_entry.lid));
		change_endianness(&(pd_entry.m));

		change_endianness(&(pd_entry.xc));
		change_endianness(&(pd_entry.yc));
		change_endianness(&(pd_entry.zc));

		change_endianness(&(pd_entry.uc));
		change_endianness(&(pd_entry.vc));
		change_endianness(&(pd_entry.wc));

		change_endianness(&(pd_entry.o1));
		change_endianness(&(pd_entry.o2));
		change_endianness(&(pd_entry.o3));

		change_endianness(&(pd_entry.r));
		change_endianness(&(pd_entry.rho));

		change_endianness(&(pd_entry.ml1));
		change_endianness(&(pd_entry.ml2));
		change_endianness(&(pd_entry.ml3));

		change_endianness(&(pd_entry.ma1));
		change_endianness(&(pd_entry.ma2));
		change_endianness(&(pd_entry.ma3));

		change_endianness(&(pd_entry.fx_c_o));
		change_endianness(&(pd_entry.fy_c_o));
		change_endianness(&(pd_entry.fz_c_o));
		
		change_endianness(&(pd_entry.mx_c_o));
		change_endianness(&(pd_entry.my_c_o));
		change_endianness(&(pd_entry.mz_c_o));
		
		particles.push_back(pd_entry);	

		pos += PARTICLE_CHANNEL_DATA_ITEM_SIZE;	// pos to next entry
		++current_particle;
	}

	delete[] data;		

	input_file.close();
	
	return true;
}

bool particle_data_manager::clearFile(std::string& file_name)
{
	std::ofstream file(file_name, std::fstream::out|std::fstream::binary|std::fstream::trunc);

	if (file.fail())	{
		return false;
	}

	file.close();

	return true;
}

bool particle_data_manager::saveTimeStepToFile(std::string& file_name, float time, std::vector<particle_own_data> &particles, bool fixed)
{
	int count = particles.size();
	
	std::fstream file(file_name, std::fstream::out|std::fstream::binary|std::fstream::app);

	// error
	if (!file.good())	{
		return false;
	}

	own_time_step_header header(count, time, fixed);
	
	// write header for timestep
	file.write(reinterpret_cast<char*>(&header.number_particle_lists), sizeof(unsigned __int32));
	file.write(reinterpret_cast<char*>(&header.vertex_data_type), sizeof(char));
	file.write(reinterpret_cast<char*>(&header.color_data_type), sizeof(char));
	file.write(reinterpret_cast<char*>(&header.globalRadius), sizeof(float));
	if (!fixed)	{
		file.write(reinterpret_cast<char*>(&header.globalRho), sizeof(float));
		file.write(reinterpret_cast<char*>(&header.globalTime), sizeof(float));
		file.write(reinterpret_cast<char*>(&header.min_max_velocity), 6*sizeof(float));
		file.write(reinterpret_cast<char*>(&header.min_max_vel_length), 2*sizeof(float));
		file.write(reinterpret_cast<char*>(&header.min_max_angle), 2*sizeof(float));
	}
	file.write(reinterpret_cast<char*>(&header.global_rgb_color), 4*sizeof(char));
	file.write(reinterpret_cast<char*>(&header.particle_count), sizeof(unsigned __int64));
	
	// write particle data for timestep
	for (int idx=0; idx < count; ++idx)	{
		file.write(reinterpret_cast<char*>(&particles[idx].xc), sizeof(float));
		file.write(reinterpret_cast<char*>(&particles[idx].yc), sizeof(float));
		file.write(reinterpret_cast<char*>(&particles[idx].zc), sizeof(float));
		if (!fixed)	{
			file.write(reinterpret_cast<char*>(&particles[idx].uc), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].vc), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].wc), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].o1), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].o2), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].o3), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].f1), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].f2), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].f3), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].f4), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].f5), sizeof(float));
			file.write(reinterpret_cast<char*>(&particles[idx].f6), sizeof(float));
		}
	}
	//file.write(reinterpret_cast<char*>(&particles.front()), count*sizeof(particle_own_data));
	
	file.close();

	return true;
}

bool particle_data_manager::addHeaderToFile(std::string& file_name, bool mmxpld)
{
	std::fstream file(file_name, std::fstream::out|std::fstream::binary|std::fstream::app);

	// error
	if (!file.good())
	{
		return false;
	}

	std::vector<float> clipping_box = boundingbox;
	for (int idx=0; idx<3; ++idx)	{
		clipping_box[idx] -= particleRadius;	// minimum - particle 
		clipping_box[idx+3] += particleRadius;	// maximum + particle radius
	}

	if (mmxpld)	{
		mmxpld_header header(number_frames, boundingbox, clipping_box);
		file.write(reinterpret_cast<char*>(&header.magic_identifier), 7*sizeof(char));
		file.write(reinterpret_cast<char*>(&header.version_number), sizeof(unsigned __int16));
		file.write(reinterpret_cast<char*>(&header.number_extra_member), sizeof(char));
		for (int i=0; i<header.number_extra_member; ++i)	{
			file.write(header.extra_member[i].data(), 4*sizeof(char));
		}
		file.write(reinterpret_cast<char*>(&header.number_data_frames), sizeof(unsigned __int32));
		file.write(reinterpret_cast<char*>(&header.bounding_box), 6*sizeof(float));
		file.write(reinterpret_cast<char*>(&header.clipping_box), 6*sizeof(float));
		file.write(reinterpret_cast<char*>(&header.min_max_velocity), 6*sizeof(float)); // placeholder - correct value has to be calculated later
		file.write(reinterpret_cast<char*>(&header.min_max_vel_length), 2*sizeof(float)); // placeholder - correct value has to be calculated later
		file.write(reinterpret_cast<char*>(&header.min_max_angle), 2*sizeof(float)); // placeholder - correct value has to be calculated later
	}
	else {
		mmpld_header header(1, boundingbox, clipping_box);
		file.write(reinterpret_cast<char*>(&header), sizeof(mmpld_header));
	}

	file.close();

	return true;
}

bool particle_data_manager::addSeekTableToFile(std::string& file_name, bool mmxpld)	{
	std::fstream file(file_name, std::fstream::out|std::fstream::binary|std::fstream::app);

	// error
	if (!file.good())
	{
		return false;
	}

	if (mmxpld)	{
		unsigned __int64 byte_offset = MMXPLDHEADERSIZE + (number_frames+1)*sizeof(unsigned __int64);	//offset from beginning of the file to the 
																											//beginning of the corresponding time frame data
		file.write(reinterpret_cast<char*>(&byte_offset), sizeof(unsigned __int64));

		for (unsigned int i = 1; i <= number_frames; ++i)	{
			byte_offset += FRAMEHEADERSIZE + 2*sizeof(float)  + (6 + 2 + 2) *sizeof(float) + sizeof(particle_own_data) * particleCount;	// TODO: Problem:particleCount �ndert sich ja
			file.write(reinterpret_cast<char*>(&byte_offset), sizeof(unsigned __int64));
		}
	}
	else {
		//mmpld -> fixed particles -> number_frames = 1
		unsigned __int64 byte_offset = sizeof(mmpld_header) + 2*sizeof(unsigned __int64);	//offset from beginning of the file to the beginning of the corresponding time frame data
		file.write(reinterpret_cast<char*>(&byte_offset), sizeof(unsigned __int64));
		
		byte_offset += FRAMEHEADERSIZE + 3*sizeof(float) * PARTICLE_CHANNEL_FIXED_COUNT;
		file.write(reinterpret_cast<char*>(&byte_offset), sizeof(unsigned __int64));
	}

	file.close();

	return true;
}

bool particle_data_manager::modifyMinMaxVelocityMinMaxAngle(std::string& file_name) {
	std::fstream file(file_name, std::fstream::out|std::fstream::binary|std::fstream::in);

	// error
	if (!file.good())
	{
		return false;
	}
	
	file.seekp(MMXPLDHEADERSIZE - (6*sizeof(float) + 2*sizeof(float) + 2*sizeof(float)));
	
	file.write(reinterpret_cast<char*>(globalMinVelocity.data()), 3*sizeof(float));
	file.write(reinterpret_cast<char*>(globalMaxVelocity.data()), 3*sizeof(float));
	file.write(reinterpret_cast<char*>(&globalMinVelLength), sizeof(float));
	file.write(reinterpret_cast<char*>(&globalMaxVelLength), sizeof(float));
	file.write(reinterpret_cast<char*>(&globalMinAngle), sizeof(float));
	file.write(reinterpret_cast<char*>(&globalMaxAngle), sizeof(float));

	file.close();

	return true;
}

particle_data_manager::mmxpld_header::mmxpld_header(int number_data_frames, std::vector<float> bounding_box, std::vector<float> clipping_box)	{
	magic_identifier[0] = 'M';	//TODO: hier string draus machen
	magic_identifier[1] = 'M';
	magic_identifier[2] = 'X';
	magic_identifier[3] = 'P';
	magic_identifier[4] = 'L';
	magic_identifier[5] = 'D';
	magic_identifier[6] = 0;
	version_number = 100;
	switch (currentDataType)	{
		case BUBBLE:	extra_member.push_back("fx ");
						extra_member.push_back("fy ");
						extra_member.push_back("fz ");
						extra_member.push_back("fxc");
						extra_member.push_back("fyc");
						extra_member.push_back("fzc");
						break;
		case CHANNEL:	extra_member.push_back("ml1");
						extra_member.push_back("ml2");
						extra_member.push_back("ml3");
						extra_member.push_back("ma1");
						extra_member.push_back("ma2");
						extra_member.push_back("ma3");
						break;
	}
	number_extra_member = extra_member.size();
	this->number_data_frames = number_data_frames;
	for (int idx=0; idx < 6; ++idx)	{// not checked: size has to be 6 TODO: auch in vector speichern
		this->bounding_box[idx] = bounding_box[idx];
		this->clipping_box[idx] = clipping_box[idx];
	}
}

particle_data_manager::mmpld_header::mmpld_header(int number_data_frames, std::vector<float> bounding_box, std::vector<float> clipping_box)	{
	magic_identifier[0] = 'M';	//TODO: hier string draus machen
	magic_identifier[1] = 'M';
	magic_identifier[2] = 'P';
	magic_identifier[3] = 'L';
	magic_identifier[4] = 'D';
	magic_identifier[5] = 0;
	version_number = 100;
	this->number_data_frames = number_data_frames;
	for (int idx=0; idx < 6; ++idx)	{// not checked: size has to be 6
		this->bounding_box[idx] = bounding_box[idx];
		this->clipping_box[idx] = clipping_box[idx];
	}
}

particle_data_manager::own_time_step_header::own_time_step_header(int particle_count, float time, bool fixed) {
	number_particle_lists = 1;
	this->particle_count = particle_count;
	vertex_data_type = 1;	//FLOAT_XYZ
	color_data_type = 0;	//NONE
	globalRadius = particleRadius;
	globalRho = particleRho;
	globalTime = time;
	for (int c=0; c<3; ++c) {
		min_max_velocity[c] = localMinVelocity[c];
		min_max_velocity[c + 3] = localMaxVelocity[c];
	}
	min_max_vel_length[0] = localMinVelLength;
	min_max_vel_length[1] = localMaxVelLength;
	min_max_angle[0] = localMinAngle;
	min_max_angle[1] = localMaxAngle;
	if (!fixed)	{
		global_rgb_color[0] = 128;
		global_rgb_color[1] = 128;
		global_rgb_color[2] = 255;
	}
	else {
		global_rgb_color[0] = 150;
		global_rgb_color[1] = 100;
		global_rgb_color[2] = 50;
	}
	global_rgb_color[3] = 255;
}