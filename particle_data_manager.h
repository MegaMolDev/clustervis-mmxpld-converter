#ifndef PARTICLE_DATA_MANAGER_H
#define PARTICLE_DATA_MANAGER_H

#include <fstream>
#include <sstream>
#include <string>
#include <vector>


class particle_data_manager
{
public:
	static const int PARTICLE_CHANNEL_DATA_ITEM_SIZE = 880;
	static const int PARTICLE_CHANNEL_DATA_HEADER_SIZE = 208;
	static const int PARTICLE_CHANNEL_FIXED_COUNT = 13500;
	static const int PARTICLE_CHANNEL_FIRST_TIMESTEP_WITHOUT_FIXED_PARTICLES = 170; // start counting at 0

	static const int PARTICLE_BUBBLE_DATA_ITEM_SIZE = 72;
	static const int PARTICLE_BUBBLE_DATA_HEADER_SIZE = 72;
	
	static const int FRAMEHEADERSIZE = sizeof(unsigned __int32) + 2*sizeof(char) + sizeof(float) + 4*sizeof(char) + sizeof(unsigned __int64);
	static const int MMXPLDHEADERSIZE = 7*sizeof(char) + sizeof(unsigned __int16) + sizeof(char) + 6*4*sizeof(char) + 
		sizeof(unsigned __int32) + 3*6*sizeof(float) + 2*sizeof(float) + 2*sizeof(float);	// TODO: flexibler schreiben
	
	enum dataTypes {BUBBLE, CHANNEL};

	struct particle_channel_data	{
		double id, lid, m,			// global id, local id, master proc -> =0
			xc, yc, zc,				// particle center
			uc, vc, wc,				// center velocity
			o1, o2, o3,				// center angular velocity
			r, rho,					// radius, density
			ml1, ml2, ml3,			// fluid moment linear
			ma1, ma2, ma3,			// fluid moment angular
			fx_c_o, fy_c_o, fz_c_o, // particle collision force previous time -> =0
			mx_c_o, my_c_o, mz_c_o; // particle collsision moment previous time -> =0
	};

	struct particle_bubble_data	{
		float tp,						// time
			xc, yc, zc,					// particle position
			r, rho,						// radius, density
			uc, vc, wc,					// center velocity
			o1, o2, o3,					// angular velocity
			fx_gl, fy_gl, fz_gl,		// forces on particles
			fx_c_gl, fy_c_gl, fz_c_gl;	// collision forces on particles	
	};

	struct particle_own_data	{
		float 			
			xc, yc, zc,				// center position of particle
			uc, vc, wc,				// center velocity of particle
			o1, o2, o3,				// center angular velocity of particle
			f1, f2, f3, f4, f5, f6;	// extra member
	};

	struct mmxpld_header	{
		char magic_identifier[7]; // hier kein pointer, da bei write alles auf char* gecastet wird
		unsigned __int16 version_number;
		char number_extra_member;
		std::vector<std::string> extra_member;
		unsigned __int32 number_data_frames;
		float bounding_box[6];
		float clipping_box[6];
		float min_max_velocity[6];
		float min_max_vel_length[2];
		float min_max_angle[2];

		mmxpld_header(int number_data_frames, std::vector<float> bounding_box, std::vector<float> clipping_box);
	};

	struct mmpld_header	{
		char magic_identifier[6];
		unsigned __int16 version_number;
		unsigned __int32 number_data_frames;
		float bounding_box[6];
		float clipping_box[6];

		mmpld_header(int number_data_frames, std::vector<float> bounding_box, std::vector<float> clipping_box);
	};

	struct own_time_step_header	{
		unsigned __int32 number_particle_lists;
		char vertex_data_type;
		char color_data_type;
		float globalRadius;
		float globalRho;
		float globalTime;	// passed time in seconds since simulation start at this time step
		float min_max_velocity[6];
		float min_max_vel_length[2];
		float min_max_angle[2];
		unsigned char global_rgb_color[4];
		unsigned __int64 particle_count;	// quantity of the particles	
		
		own_time_step_header(int particle_count, float time, bool fixed = false);
	};

	particle_data_manager(void);
	~particle_data_manager(void);

	template<class T> inline void change_endianness(T * value);
	
	bool convertChannelToOwn(particle_channel_data& from_rec, particle_own_data& to_rec);
	bool convertBubbleToOwn(particle_bubble_data& from_rec, particle_own_data& to_rec);

	bool convertChannel(std::string& from_path, std::string& own_to_file, int start_time_step, int end_time_step);
	bool convertBubble(std::string& from_path, std::string& own_to_file, int start_time_step, int end_time_step);
	
	bool loadTimestepTimes(std::string& file_name, std::vector<double> &times);

	bool loadFromChannelFile(std::string& folder, int time_step, std::vector<particle_channel_data> &particles);
	bool loadFromBubbleFile(std::string& folder, int time_step, std::vector<particle_bubble_data> &particles);
	
	bool clearFile(std::string& file_name);
	bool saveTimeStepToFile(std::string& file_name, float time, std::vector<particle_own_data> &particles, bool fixed = false);
	bool addHeaderToFile(std::string& file_name, bool mmxpld = true);
	bool addSeekTableToFile(std::string& file_name, bool mmxpld = true);
	bool modifyMinMaxVelocityMinMaxAngle(std::string& file_name);

private:
	unsigned int number_frames;

	std::vector<float> boundingbox;	// contains {minX, minY, minZ, maxX, maxY, maxZ}
	static float particleRadius;
	static float particleRho;

	static dataTypes currentDataType;

	int particleCount;	// number of (output) particles per timestep

	std::vector<float> globalMinVelocity;
	std::vector<float> globalMaxVelocity;
	float globalMinVelLength;
	float globalMaxVelLength;
	float globalMinAngle;
	float globalMaxAngle;

	static std::vector<float> localMinVelocity;
	static std::vector<float> localMaxVelocity;
	static float localMinVelLength;
	static float localMaxVelLength;
	static float localMinAngle;
	static float localMaxAngle;
};


#endif