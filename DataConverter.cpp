#include <string>
#include "particle_data_manager.h"

int main()	//TODO: Argumente entgegennehmen und benutzen
{
	// do convert
	bool res;

	particle_data_manager::dataTypes current_dataType = particle_data_manager::BUBBLE;

	particle_data_manager pdm = particle_data_manager();

	switch (current_dataType)	{
		case particle_data_manager::CHANNEL: res = pdm.convertChannel(std::string("D:\\Uni\\Diplom\\data\\Kanal"),	//Inputfolder
										std::string("D:\\Uni\\Diplom\\source\\MegaMol\\data\\kanal_mobile.mmxpld"), //Outputfolder
										1,	//first timestep
										427	//last timestep
										);
					break;
		/*case particle_data_manager::BUBBLE:res = pdm.convertBubble(std::string("D:\\Uni\\Diplom\\data\\Blasen"),	//Inputfolder
										std::string("D:\\Uni\\Diplom\\source\\MegaMol\\data\\blasen_part.mmxpld"), //Outputfolder
										25000,	//first timestep
										26000//58515	//last timestep
										);
					break;*/
		case particle_data_manager::BUBBLE:res = pdm.convertBubble(std::string("D:\\Uni\\Diplom\\data\\Blasen_neu"),	//Inputfolder
							std::string("D:\\Uni\\Diplom\\source\\MegaMol\\data\\blasen_neu.mmxpld"), //Outputfolder
							361,	//first timestep
							559//58515	//last timestep
							);
		break;
	}

	if (!res)	{
		return -1;
	}

	getchar();
	
	return 0;
}